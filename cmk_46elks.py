#!/usr/bin/python3
# SMS (using 46elks)
# Notifications from check_mk via sms using the 46elks SMS REST API
#
# Created by @mrw00t Viktor Alakörkkö in 2019
# https://www.linkedin.com/in/alakorkko/
#
# License CC0 1.0
# https://creativecommons.org/publicdomain/zero/1.0/
# 
# For instalation se official check_mk documentation
# https://checkmk.com/cms_notifications.html#scripts
#

import os
import requests

#46elks API user and API password
username = '<API Username>'
api_password = '<API Password>'

#Build the message contents from the NOTIFY_ data
max_len = 160
message = os.environ['NOTIFY_HOSTNAME'] + " "

if os.environ['NOTIFY_WHAT'] == 'SERVICE':
    message += os.environ['NOTIFY_SERVICESTATE'][:2] + " "
    avail_len = max_len - len(message)
    message += os.environ['NOTIFY_SERVICEDESC'][:avail_len] + " "
    avail_len = max_len - len(message)
    message += os.environ['NOTIFY_SERVICEOUTPUT'][:avail_len]

else:
    message += "is " + os.environ['NOTIFY_HOSTSTATE']

#Get the phone number from the emergency pager feild in check_mk
pager = os.environ['NOTIFY_CONTACTPAGER']

#Structure authentication credentials for the request module
auth = (username, api_password)

#Fields for the API call
fields = {
    'from': 'CheckMK',
    'to': pager,
    'message': message,
    'flashsms': "no",
    'dontlog': "message"
}

#Make the API call and print the response
response = requests.post("https://api.46elks.com/a1/SMS", data=fields, auth=auth)
print(response.text)